brew reinstall ruby
/opt/homebrew/opt/ruby/bin/ruby -v
brew install rbenv ruby-build
rbenv init
rbenv install 3.1.2
rbenv global 3.1.2
echo 'eval "$(rbenv init - zsh)"' >> ~/.zshrc
echo 'export PATH="$HOME/.gem/ruby/3.1.2/bin:$PATH"' >> ~/.zshrc
gem env home /Users/xx/.rbenv/versions/3.1.2/lib/ruby/gems/3.1.2
sudo gem install ffi:1.15.5

sudo gem install bundler
bundle install
bundle exec jekyll build