---
layout: post
title: Vie de glace
image: Vie de glace
pictorem_url: 339356/Vie%20de%20glace.html
gumroad_url: https://gum.co/RNrUj
eyeem_url: https://www.eyeem.com/p/168031571
tags: accueil vertical noirblanc archives abstrait textures
collections: []
livres: Textures
---
*Les formes de la glace, façonnée par l'eau et le sel.*

*- Février 2016*
