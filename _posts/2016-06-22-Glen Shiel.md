---
layout: post
title: Glen Shiel
image: Glen Shiel
pictorem_url: 339366/Glen%20Shiel.html
eyeem_url: https://www.eyeem.com/p/168031558
tags: accueil 2x1 voyages europe archives paysages
collections: []
---
*Cette valée si majestueuse, ou les arbres et les nuages dansent en harmonie.*

*- Juin 2016*