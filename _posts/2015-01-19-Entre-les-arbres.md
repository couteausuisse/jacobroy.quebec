---
layout: post
title: Entre les arbres
image: Entre les arbres
pictorem_url: 339358/Entre%20les%20arbres.html
eyeem_url: https://www.eyeem.com/p/168031554
tags: accueil quebec archives
collections: []
---
*Bien campé dans mes skis, sous une forte tempête, une porte s'ouvre entre les arbres.*

*- Janvier 2015*