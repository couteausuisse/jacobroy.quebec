---
layout: post
title: GNISIRPU
image: GNISIRPU
pictorem_url: 339361/GNISIRPU.html
eyeem_url: https://www.eyeem.com/p/168031559
tags: accueil quebec archives abstrait
collections: []
---
*En tant que peuple, nous avons le droit et l'obligation de changer le cours de l'histoire. Il nous reste qu'à nous lever.*

*- Octobre 2017*