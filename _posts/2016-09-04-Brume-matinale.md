---
layout: post
title: Brume matinale
image: Brume matinale
pictorem_url: 339365/Brume%20matinale.html
eyeem_url: https://www.eyeem.com/p/168031548
tags: accueil 2x1 noirblanc quebec archives paysages
collections: ["2x1"]
---
*La brume se lève tranquillement, en suivant le jour.*

*- Semptembre 2016*