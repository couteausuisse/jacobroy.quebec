---
layout: post
title: Creux du Van
image: Creux du Van
pictorem_url: 339371/Creux%20du%20Van.html
eyeem_url: https://www.eyeem.com/p/168031551
tags: accueil 2x1 voyages europe archives paysages
collections: []
---
*Dans l'humide.*

*En Suisse, traînant tous nos effets sur notre dos, nous avons entrepris la magnifique randonnée de Creux du Van.*

*- Juin 2016*