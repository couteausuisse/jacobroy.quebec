---
layout: post
title: Murchison Memorial
image: Murchison Memorial
pictorem_url: 339364/Murchison%20Memorial.html
eyeem_url: https://www.eyeem.com/p/168031562
tags: accueil voyages europe archives paysages
collections: []
---
*En Écosse, juste avant de traverser vers Ile Of Skye se cache un monument avec une vue imprenable.*

*- Juin 2016*